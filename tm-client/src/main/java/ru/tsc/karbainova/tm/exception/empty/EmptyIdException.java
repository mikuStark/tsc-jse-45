package ru.tsc.karbainova.tm.exception.empty;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {
    public EmptyIdException() {
        super("Error ID.");
    }

    public EmptyIdException(String value) {
        super("Error ID. " + value);
    }
}
