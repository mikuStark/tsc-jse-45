package ru.tsc.karbainova.tm.service.model;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.dto.IService;
import ru.tsc.karbainova.tm.model.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> {

    @NonNull
    protected final IConnectionService connectionService;

    protected AbstractService(@NonNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
