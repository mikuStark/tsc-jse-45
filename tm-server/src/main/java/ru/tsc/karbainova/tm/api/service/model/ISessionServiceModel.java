package ru.tsc.karbainova.tm.api.service.model;

import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.model.Session;

import java.util.List;

public interface ISessionServiceModel {
    boolean checkDataAccess(String login, String password);

    @SneakyThrows
    Session add(Session session);

    @SneakyThrows
    Session open(String login, String password);

    Session sign(Session session);

    @SneakyThrows
    List<Session> findAll();

    List<Session> getListSessionByUserId(String userId);

    @SneakyThrows
    void validate(Session session);

    void validate(Session session, Role role);

    @SneakyThrows
    void close(Session session);

    void closeAll(Session session);

    void signOutByUserId(String userId);
}
