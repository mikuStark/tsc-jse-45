package ru.tsc.karbainova.tm.exception.empty;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class EmptyUserNotFoundException extends AbstractException {
    public EmptyUserNotFoundException() {
        super("Error User Not Found.");
    }

    public EmptyUserNotFoundException(String value) {
        super("Error User Not Found. " + value);
    }
}
